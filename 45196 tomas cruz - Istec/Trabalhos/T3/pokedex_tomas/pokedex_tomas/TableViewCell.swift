//
//  TableViewCell.swift
//  pokedex_tomas
//
//  Created by Developer on 02/07/2019.
//  Copyright © 2019 Tomás Dias. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var pimage: UIImageView!
    @IBOutlet weak var pname: UILabel!
    @IBOutlet weak var ptipo: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
