//
//  ViewController.swift
//  pokedex_tomas
//
//  Created by Developer on 02/07/2019.
//  Copyright © 2019 Tomás Dias. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{

    @IBOutlet weak var table: UITableView!
    
    var pokemons = [pokemon]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setuppokemon()
    }
    
    private func setuppokemon(){
        pokemons.append(pokemon(name:"pikachu", image: "1", tipo: .ground))
        pokemons.append(pokemon(name:"pikachu", image: "1", tipo: .ground))
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pokemons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? TableViewCell else{
            return UITableViewCell()
        }
        cell.pname.text = pokemons[indexPath.row].name
        cell.ptipo.text = pokemons[indexPath.row].tipo.rawValue
        cell.pimage.image = UIImage(named:pokemons[indexPath.row].image)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 175
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        guard editingStyle == .delete else { return }
        pokemons.remove(at: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: .automatic)
    }
    
}

class pokemon {
    let name: String
    let image: String
    let tipo: Pokemontype
    
    init(name: String, image: String, tipo: Pokemontype) {
        self.name = name
        self.image = image
        self.tipo = tipo
        
    }
    
}

enum Pokemontype: String{
    case water = "water"
    case fire = "fire"
    case ground = "ground"
    case air = "air"
}
