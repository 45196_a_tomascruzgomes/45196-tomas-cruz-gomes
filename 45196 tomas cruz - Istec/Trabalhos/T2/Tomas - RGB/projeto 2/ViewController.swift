//
//  ViewController.swift
//  projeto 2
//
//  Created by Developer on 14/05/2019.
//  Copyright © 2019 ISTECaluno. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

   
    /*var colorlable:UILabel?
    var colordisplaylable:UILabel?
    var redslider:UISlider?
    var greenslider:UISlider?
    var blueslider:UISlider?
    var alphaslider:UISlider?*/
    
    @IBOutlet weak var colorlable: UILabel!
    @IBOutlet weak var colordisplaylable: UILabel!
    @IBOutlet weak var redslider: UISlider!
    @IBOutlet weak var greenslider: UISlider!
    @IBOutlet weak var blueslider: UISlider!
    @IBOutlet weak var alphaslider: UISlider!
    
    lazy var arr:[UIView?] = [colorlable, colordisplaylable, redslider, greenslider, blueslider, alphaslider]


    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*self.colorlable = UILabel(frame: CGRect(x: 85, y: 150, width: 200, height: 200))
        //self.colorlable?.text = "Ola Mundo"
        //self.colorlable?.backgroundColor = UIColor.brown
        self.colorlable?.textColor = UIColor.white
        self.colorlable?.textAlignment = NSTextAlignment.center
        self.colorlable?.font = UIFont(name: "MarkerFelt-Thin", size: 30)
       
        self.colordisplaylable = UILabel(frame: CGRect(x: 30, y: 400, width: 325, height: 50))
        //self.colordisplaylable?.text = ""
        //self.colordisplaylable?.backgroundColor = UIColor.brown
        self.colordisplaylable?.textColor = UIColor.white
        self.colordisplaylable?.textAlignment = NSTextAlignment.center
        self.colordisplaylable?.font = UIFont(name: "MarkerFelt-Thin", size: 30)
        
        self.redslider = UISlider(frame: CGRect(x: 30, y: 500, width: 325, height: 1))
        
        self.redslider?.thumbTintColor = UIColor.red
        self.redslider?.maximumValue = 255
        self.redslider?.minimumValue = 0
        self.redslider?.isContinuous = true
        self.redslider?.setValue(50, animated: false)
        
        self.greenslider = UISlider(frame: CGRect(x: 30, y: 550, width: 325, height: 1))
        self.greenslider?.setValue(50, animated: false)
        self.greenslider?.thumbTintColor = UIColor.green
        self.greenslider?.maximumValue = 255
        self.greenslider?.minimumValue = 0
        
        self.blueslider = UISlider(frame: CGRect(x: 30, y: 600, width: 325, height: 1))
        self.blueslider?.setValue(50, animated: false)
        self.blueslider?.thumbTintColor = UIColor.blue
        self.blueslider?.maximumValue = 255
        self.blueslider?.minimumValue = 0
        
        self.alphaslider = UISlider(frame: CGRect(x: 30, y: 650, width: 325, height: 1))
        self.alphaslider?.setValue(50, animated: false)
        self.alphaslider?.thumbTintColor = UIColor.black
        self.alphaslider?.maximumValue = 1
        self.alphaslider?.minimumValue = 0
 
        for vista in arr
        {
            if let v = vista
            {
                self.view.addSubview(v)
            }
         }*/

    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func changebackgroundcolor() {
    let red = CGFloat((redslider?.value)!)
    let green = CGFloat((greenslider?.value)!)
    let blue = CGFloat((blueslider?.value)!)
    let alpha = CGFloat((alphaslider?.value)!)
    
        colorlable?.backgroundColor = UIColor(red: red, green: green, blue: blue, alpha: alpha)
    
        colordisplaylable?.text = "red: \(red) green: \(green) blue: \(blue) alpha: \(alpha)"
    }
}

